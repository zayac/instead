import telebot
import config
from telebot import types
from pymongo import MongoClient
import pymongo
import re
from runtime.main import InsteadRuntime


instead = InsteadRuntime()
bot = telebot.TeleBot(config.token)
db = MongoClient().instead


class Player():
    ''' Класс пользователя. Хранит информацию о пользователе
        и о его текущем состоянии в игре.
    '''
    def __init__(self, message):
        ''' Если пользователь есть в БД, то обновляет данные о нем и создает
        экземпляр класса из БД, если пользователя в БД нет,
        то создает экземпляр класса и добавляет его в бд.
        Args:
            message (class): Ответ телеграмма с информацией об отправителе
                сообщения.
        '''
        self.user_id = message.from_user.id
        if db.players.find_one({'_id': self.user_id}):
            db.players.find_one_and_update(
                {'_id': self.user_id}, {'$set': {
                    'user.last_name': message.from_user.last_name,
                    'user.first_name': message.from_user.first_name,
                    'user.username': message.from_user.username,
                }})
            print('''Player - Пользователь {0} найден'''.format(self.user_id))
        else:
            # self.curent_location = 'main'
            self.user = {
                'last_name': message.from_user.last_name,
                'first_name': message.from_user.first_name,
                'username': message.from_user.username,
            }
            dict_add = self.create_dict()
            try:
                db.players.insert(dict_add)
                print('''Player - Пользователь создан''')
            except pymongo.errors.DuplicateKeyError:
                print('''Player - Пользователь {0} существует
'''.format(self.user_id))

        self.obj_user = db.players.find_one({'_id': self.user_id})
        self.username = self.obj_user.get('user')
        # self.curent_location = self.obj_user.get('curent_location')
        self.callback_value = self.obj_user.get('callback_value')
        self.inventory = self.obj_user.get('inventory')

    def restart(self):
        ''' Обнуляет положение игрока. Рестарт игры.
        '''
        room = 'main'
        inventory = None
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                # 'curent_location': room,
                'callback': None,
                'inventory': inventory
            }})
        print('''Player -  Значения игрока сброшены(Рестарт игры)''')

    def update_callback(self, callback_value):
        ''' Обновляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'callback': callback_value
            }})
        self.callback_value = callback_value
        print('''Player -  Значения callback обновленны''')

    def reset_callback(self):
        ''' Обнуляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'callback': None
            }})
        self.callback_value = None
        print('''Player -  Значения callback обнуленны''')

    def create_dict(self):
        ''' Возвращает словарь для записи в бд.
        Args:
            _id (int): id пользователя, которое будет его-же id в бд
            user (dict): Хранит значения имени, фамилии, логина
            curent_location (str): Текущая локация игрока.
        Returns:
            dict_add (tuple): Кортеж внутри которого лежит словарь.
        '''
        inventory = None
        dict_add = ({
            '_id': self.user_id,
            'user': self.user,
            # 'curent_location': self.curent_location,
            'callback': None,
            # Нужна функция для получения инвентаря из инстеад, и составление из него списка.
            # Поскольку не все игры начинаются с пустым инвентарем.
            'inventory': inventory
        })
        print('''Player -  Словарь для записи в БД создан''')
        return dict_add

    def debug(self):
        print('''
Player - ПРОВЕРКА ЗНАЧЕНИЙ
Сallback Value: {0}
Inventory: {1}
'''.format(self.callback_value, self.inventory))


class Room():
    """ Управлят процессами в комнате"""

    action_re = re.compile(r'\{(\w+)\|(\w+)\}')

    def __init__(self):  # curent_location
        # self.curent_location = curent_location
        self.nam = instead.title()
        self.no_proccess_dsc = instead.description()
        self.way = instead.ways()

    def debug(self):
        """ Функция для дебага
            Сомтрим интересющие нас состоаяния данного класса."""
        print('''
Room - ПРОВЕРКА ЗНАЧЕНИЙ
Текущая комната: {0}
Название комнаты: {1}
Описание комнаты: {2}
Пути в комнате: {3}
'''.format(self.nam, self.no_proccess_dsc, self.way))  # self.curent_location,

    def process_dsc(self):
        """ Получаем отформатированное описание комнаты """
        print('''Room - Форматируем описание комнаты''')
        self.dsc = self.format_description(self.no_proccess_dsc)
        splittext = self.dsc.split()
        self.dsc = ' '.join(splittext)
        self.dsc = self.dsc.replace("^", "\n")
        self.dsc = self.dsc.replace("--", "—")

    def format_action(self, match):
        ''' Форматирует отдельно взятую команду.
        Args:
            match: совпадение регулярного выражния, объект match
        Returns:
            Строка вида "описание (\name)"
        '''
        return "{0} (/{1})".format(match.group(2), match.group(1))

    def format_description(self, text):
        ''' Форматирует действия в описании сцены и объектов
        Args:
            text (str): Текст, который нужно отформатировать
        Returns:
            Строка, в которой все команды вида "{name|описание}"
             заменены на "описание (\name)"
        '''
        return self.action_re.sub(self.format_action, text)


class Game():
    """ Управляет процессом игры """
    def __init__(self, pl, message):
        self.pl = pl
        self.message = message
        # self.curent_location = pl.curent_location
        self.rm = Room()#self.curent_location
        self.pasting_text()

    def pasting_text(self):
        ''' Подготавливает сообщение для отправки в телеграмм
        Returns:
            text (str): Готовое сообщение для отправки
        '''
        print('''Room - Подготавливаем описание комнаты''')
        self.rm.process_dsc()
        self.text = '{0}\n\n{1}'.format(
            self.rm.nam, self.rm.dsc)

    def change_location(self, room_ident):
        # self.pl.curent_location = room_ident
        print(room_ident)
        instead.walk(room_ident)

#     def update_player(self):
#         ''' Обновляет данные в БД о Player
#         '''
#         user_id = self.pl.user_id
#         db.players.find_one_and_update(
#             {'_id': user_id}, {'$set': {
#                 # 'curent_location': self.pl.curent_location
#             }})
# #         print("""\nGame - Обновленно:
# # Комната: {0}""".format(self.pl.curent_location))


class Bot():
    """ """
# по умолчанию g - экземпляр игры не создан
    g = None
# по умолчанию pl - экземпляр игрока не создан
    pl = None
# inline_keyboard = False

    def create_player(self, message):
        """ Создание экземпляра игрока
        Args:
            message: сообщение пользователя в телеграмме"""
        if self.pl is None:
            self.pl = Player(message)
            print('Bot - создал pl')
            # print('Bot - текущая комната: ', self.pl.curent_location)

    def create_game(self, message):
        """ Создание экземпляра игры
        Args:
            message: сообщение пользователя в телеграмме
        """
        if self.g is None:
            self.g = Game(self.pl, message)
            print('Bot - создал g')

    def processing_bot_command(self, message):
        """ Обработчик стандартных команд бота, определяет команду
            и вызывает функции для исполнения полученной команды
        Args:
            message: сообщение пользователя в телеграмме
        """
        command = message.text
        print('bot_command:', message.text)
        self.create_player(message)
        self.create_game(message)
        if command == '/start':
            self.debager(message)
        elif command == '/restart':
            print('Перезапустить игру')
            self.pl.restart()
            self.g = None
            self.pl = None
            bot.send_message(message.chat.id, text='Для продолжения нажмите /start')

    def out_message(self, message, text, message_type='TYPE_ROOM'):
        """ Отправлет сообщения в телеграмм """
        chat = message.chat.id
        self.pl.reset_callback()
        if message_type == 'TYPE_ROOM':
            self.creating_inline_keyboard()
            bot.send_message(chat, text, reply_markup=self.inkeyboard)

        elif message_type == 'TYPE_NOTIFICATION':
            keyboard_hider = types.ReplyKeyboardHide()
            bot.send_message(chat, text, reply_markup=keyboard_hider)

    def creating_inline_keyboard(self):
        """ Создает инлайн клавиатуру """
        way = self.g.rm.way
        if way:
            self.inkeyboard = types.InlineKeyboardMarkup()
            locations = types.InlineKeyboardButton(text='Смена локации', callback_data="change_location")
            self.inkeyboard.row(locations)
            print('Bot - инлайн клавиатура создана:', way)
            self.create_nam('WAY')
        else:
            self.inkeyboard = types.InlineKeyboardMarkup()
            print('Bot - инлайн клавиатура не создана:', way)

    def process_callback(self, call):
        """ Обрабатывает полученные сообщения от инлайн клавиатуры """
        self.create_player(call)
        self.create_game(call)
        self.creating_inline_keyboard()
        chat = call.message.chat.id
        if call.data == 'change_location':
            callback_value = 'change_location'
            self.pl.update_callback(callback_value)
            keyboard_edit = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            for item in self.room_ident.values():
                keyboard_edit.add(item)
            bot.send_message(chat, text='Куда идем?', reply_markup=keyboard_edit)
            print('Bot - Клавиатура с ответом создана:', self.room_ident)

    def process_message(self, message):
        """ Обрабатывает полученные сообщения от пользователя """
        self.create_player(message)
        self.create_game(message)
        self.creating_inline_keyboard()
        self.pl.debug()
        if self.pl.callback_value == 'change_location':
            inv_d = {value: key for key, value in self.room_ident.items()}
            if inv_d.get(message.text):
                print('Bot - Принятое сообщение с клавиатуры: ', message.text[0: 10])
                text = 'Вы перешли в %s' % message.text
                location = inv_d.get(message.text)
                print('Game - комната для смены: ', location)
                self.g.change_location(location)
                # self.g.update_player()
                self.out_message(message, text, message_type='TYPE_NOTIFICATION')
    # Обнулим g и pl, во избежании недоразуменй с неочищеной паматью.
                self.g = None
                self.pl = None
                self.create_player(message)
                self.create_game(message)
                self.out_message(message, self.g.text)
            else:
                text = 'Выберете значение с клавиатуры'
                bot.send_message(message.chat.id, text)
        # elif self.pl.callback_value == 'inventory':
        #     inv_d = {value: key for key, value in self.nam_inventory.items()}
        #     if inv_d.get(message.text):
        #         print('Bot - Принятое сообщение с клавиатуры: ', message.text)
        #         selected_obj = inv_d.get(message.text)
        #         text = objects.get(selected_obj).get('inv')
        #         self.out_message(message, text, message_type='TYPE_NOTIFICATION')
        #         self.pl.reset_callback()
        #     else:
        #         text = 'Выберете значение с клавиатуры'
        #         bot.send_message(message.chat.id, text)
        else:
            text = 'Что-то пошло не так...'
            bot.send_message(message.chat.id, text)

    def create_nam(self, TYPE_NAM):
        """ Cоздает список вида
        {идентификатор: человекочитаемое_название}
            Args: TYPE_NAM - тип создаваемого списка, либо
                пути из комнаты, либо описание предметов.
        """
        if TYPE_NAM == 'WAY':
            self.room_ident = {}
            way = self.g.rm.way
            for item in way:
                room_ident = item[0]
                nam_room = item[1]
                p = {room_ident: nam_room}
                self.room_ident.update(p)
                print(item)

    def debager(self, message):
        """ Пока использую, как дебагер"""
        self.pl.debug()
        self.out_message(message, self.g.text)


Bot = Bot()

bot_command = ['start', 'restart']


@bot.message_handler(commands=bot_command)
def handler(message):
    # if message.text.startswith('/'):
    Bot.processing_bot_command(message)


@bot.callback_query_handler(func=lambda call: True)
def handler_callback(call):
    Bot.process_callback(call)


@bot.message_handler(func=lambda message: True)
def handler_message(message):
    Bot.process_message(message)


if __name__ == '__main__':
    bot.polling(none_stop=True)
