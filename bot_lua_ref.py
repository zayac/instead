import telebot
import config
import pymongo
import re
from telebot import types
from pymongo import MongoClient
from runtime.main import InsteadRuntime

db = MongoClient().instead
bot = telebot.TeleBot(config.token)


class Player():
    '''
    self.callback_value - Значение callback,
    чтобы знать какую клавиатуру показывать.
    self.user_id - id пользователя.
    message - Ответ телеграмма с информацией об отправителе сообщения.
    user_info - Словарь. Содержит информацию о пользователе
        last_name - Фамилия
        first_name - Имя
        username - Логин
    game_info - Словарь. Содержит информацию о игре пользователя.
        auto_save - Автосохранение для Instead.
        callback - Сallback, чтобы знать какую клавиатуру показывать
    '''
    def __init__(self, message):
        '''
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            Создает self.user_id
        '''
        self.user_id = message.from_user.id
        self.inventory = None
        self.callback_value = None

    def search_user(self):
        ''' Осуществляет поиск пользователя в DB
        Returns:
            True : Если пользователь добавлен.
            False : Если пользователь НЕ добавлен.'''
        if db.players.find_one({'_id': self.user_id}):
            print('Player - Пользователь {0} найден.'.format(self.user_id))
            return True
        else:
            print('Player - Пользователь НЕ найден.')
            return False

    def add_user(self):
        ''' Добавляет пользовтеля в DB.
        Returns:
            True : Если пользователь добавлен.
            False : Если пользователь НЕ добавлен.
        '''
        try:
            db.players.insert(self.dict_add)
            print('''Player - Пользователь {0} добавлен
'''.format(self.user_id))
            return True
        except pymongo.errors.DuplicateKeyError:
            print('''Player - Пользователь {0} существует
'''.format(self.user_id))
            return False

    def update_user(self, message):
        ''' Обновляет данные о пользователе (user_info)
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.'''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'user_info.last_name': message.from_user.last_name,
                'user_info.first_name': message.from_user.first_name,
                'user_info.username': message.from_user.username,
            }})
        print('''Player - Личные данные пользователя {0} обновленны
'''.format(self.user_id))

    def reset_game_user(self):
        ''' Обнуляет все данные о игре пользователя (game_info) '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.auto_save': None,
                'game_info.callback': None,
            }})
        print('''Player - Данные о игре пользователя {0} сброшены
'''.format(self.user_id))

    def reset_callback(self):
        ''' Обнуляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.callback': None,
            }})
        self.callback_value = None
        print('''Player - callback обнулен''')

    def update_callback(self, callback_value):
        ''' Обновляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.callback': callback_value,
            }})
        self.callback_value = callback_value
        print('''Player - callback обновлен''')

    def create_dict_add(self, message):
        ''' Создает словарь для добавления в DB.
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.'''
        user_info = {
            'last_name': message.from_user.last_name,
            'first_name': message.from_user.first_name,
            'username': message.from_user.username,
        }

        game_info = {
            'auto_save': None,
            'callback': None,
        }

        self.dict_add = {
            '_id': self.user_id,
            'user_info': user_info,
            'game_info': game_info,
        }
        print('''Player - Словарь для добавления создан''')

    def save(self, savedata):
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.auto_save': savedata,
            }})

    def load(self):
        obj_user = db.players.find_one({'_id': self.user_id})
        savedata = obj_user.get('game_info')
        data = savedata.get('auto_save')
        return data

    def debug(self):
        print('''
Player - ПРОВЕРКА ЗНАЧЕНИЙ
Сallback Value: {0}
Inventory: {1}
'''.format(self.callback_value, self.inventory))


class Room():
    """docstring for Room"""

    action_re = re.compile(r'\{(\w+)\|(\w+)\}')

    def __init__(self, g):
        self.nam = g.instead.title()
        self.no_proccess_dsc = g.instead.full_description()
        self.way = g.instead.ways()

    def debug(self):
        """ Функция для дебага
            Сомтрим интересющие нас состоаяния данного класса."""
        print('''
Room - ПРОВЕРКА ЗНАЧЕНИЙ
Текущая комната: {0}
Название комнаты: {1}
Описание комнаты: {2}
Пути в комнате: {3}
'''.format(self.nam, self.no_proccess_dsc, self.way))

    def process_dsc(self):
        """ Получаем отформатированное описание комнаты """
        print('''Room - Форматируем описание комнаты''')
        self.dsc = self.format_description(self.no_proccess_dsc)
        split_text = self.dsc.split()
        self.dsc = ' '.join(split_text)
        self.dsc = self.dsc.replace("^", "\n")
        self.dsc = self.dsc.replace("--", "—")

    def format_action(self, match):
        ''' Форматирует отдельно взятую команду.
        Args:
            match: совпадение регулярного выражния, объект match
        Returns:
            Строка вида "описание (\name)"
        '''
        return "{0} (/{1})".format(match.group(2), match.group(1))

    def format_description(self, text):
        ''' Форматирует действия в описании сцены и объектов
        Args:
            text (str): Текст, который нужно отформатировать
        Returns:
            Строка, в которой все команды вида "{name|описание}"
             заменены на "описание (\name)"
        '''
        return self.action_re.sub(self.format_action, text)


class Game():
    """docstring for Game"""
    def __init__(self, pl, message):
        self.instead = InsteadRuntime()
        self.pl = pl
        self.message = message
        self.avto_load_game()
        self.rm = Room(self)
        self.pasting_text()

    def pasting_text(self):
        ''' Подготавливает сообщение для отправки в телеграмм
        Returns:
            text (str): Готовое сообщение для отправки
        '''
        print('''Room - Подготавливаем описание комнаты''')
        self.rm.process_dsc()
        self.text = '{0}\n\n{1}'.format(
            self.rm.nam, self.rm.dsc)

    def change_location(self, room_ident):
        print(room_ident)
        self.instead.walk(room_ident)

    def avto_load_game(self):
        data = self.pl.load()
        if data:
            self.instead.load(data)
            print('Сохранение загружено')
        else:
            print('Сохранение не найдено')

    def save_game(self):
        data = self.instead.save()
        self.pl.save(data)
        print('''Game - Игра сохранена''')


class Bot():

    pl = None
    g = None

    def create_player(self, message):
        ''' Создает экземпляр пользователя
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            pl(class): Экземпляр пользователя
        '''
        # print(self.pl, self.g)
        if self.pl is None:
            self.pl = Player(message)
            if self.pl.search_user() is True:
                self.pl.update_user(message)
                print('create_player')
            else:
                self.pl.create_dict_add(message)
                self.pl.add_user()

    def create_game(self, message):
        ''' Создает экземпляр игры
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            g(class): Экземпляр игры
        '''
        if self.g is None:
            self.g = Game(self.pl, message)
            print('Bot - создал g')

    def restart_game(self, message):
        ''' Рестарт Игры
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            Обнуляет данные о игре пользователя.
        '''
        self.create_player(message)
        self.pl.reset_game_user()

    def process_command(self, message):
        ''' Обработка команд
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        '''
        command = message.text[1:]
        print('bot_command:', command)
        if command in bot_command:
            if command == 'start':
                self.create_player(message)
                self.create_game(message)
                self.debager(message)
            elif command == 'restart':
                self.restart_game(message)
        else:
            self.process_game_command(message)

    def process_game_command(self, message):
        command = message.text[1:]
        result = self.g.instead.action(command)
        text = result[0]
        print(text)
        self.g.save_game()
        message_type = 'TYPE_NOTIFICATION'
        self.out_message(message, text, message_type)

    def out_message(self, message, text, message_type='TYPE_ROOM'):
        """ Отправлет сообщения в телеграмм
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
            text(str): Сообщение для отправки в телеграмм
            message_type(str): Тип отправлемого сообщения
        """
        chat = message.chat.id
        self.pl.reset_callback()
        if message_type == 'TYPE_ROOM':
            self.creating_inline_keyboard()
            bot.send_message(chat, text, reply_markup=self.inkeyboard)

        elif message_type == 'TYPE_NOTIFICATION':
            keyboard_hider = types.ReplyKeyboardHide()
            bot.send_message(chat, text, reply_markup=keyboard_hider)
            # pass

    def creating_inline_keyboard(self):
        """ Создает инлайн клавиатуру
        Returns:
            inkeyboard : Клавиатура для отправки в телеграмм
            """
        way = self.g.rm.way
        if way:
            self.inkeyboard = types.InlineKeyboardMarkup()
            locations = types.InlineKeyboardButton(text='Смена локации', callback_data="change_location")
            self.inkeyboard.row(locations)
            print('Bot - инлайн клавиатура создана:', way)
            self.create_nam('WAY')
        else:
            self.inkeyboard = types.InlineKeyboardMarkup()
            print('Bot - инлайн клавиатура не создана:', way)

    def process_callback(self, call):
        """ Обрабатывает полученные сообщения от инлайн клавиатуры """
        self.create_player(call)
        self.create_game(call)
        self.creating_inline_keyboard()
        chat = call.message.chat.id
        if call.data == 'change_location':
            callback_value = 'change_location'
            self.pl.update_callback(callback_value)
            keyboard_edit = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            for item in self.room_ident.values():
                keyboard_edit.add(item)
            bot.send_message(chat, text='Куда идем?', reply_markup=keyboard_edit)
            print('Bot - Клавиатура с ответом создана:', self.room_ident)

    def create_nam(self, TYPE_NAM):
        """ Cоздает список вида
        {идентификатор: человекочитаемое_название}
            Args: TYPE_NAM - тип создаваемого списка, либо
                пути из комнаты, либо описание предметов.
        """
        if TYPE_NAM == 'WAY':
            self.room_ident = {}
            way = self.g.rm.way
            for item in way:
                room_ident = item[0]
                nam_room = item[1]
                p = {room_ident: nam_room}
                self.room_ident.update(p)
                print(item)

    def process_message(self, message):
        """ Обрабатывает полученные сообщения от пользователя """
        self.create_player(message)
        self.create_game(message)
        self.creating_inline_keyboard()
        self.pl.debug()
        if self.pl.callback_value == 'change_location':
            inv_d = {value: key for key, value in self.room_ident.items()}
            if inv_d.get(message.text):
                print('Bot - Принятое сообщение с клавиатуры: ', message.text[0: 10])
                text = 'Вы перешли в %s' % message.text
                location = inv_d.get(message.text)
                print('Game - комната для смены: ', location)
                self.g.change_location(location)
                self.out_message(message, text, message_type='TYPE_NOTIFICATION')
            else:
                text = 'Выберете значение с клавиатуры'
                bot.send_message(message.chat.id, text)
        else:
            text = 'Что-то пошло не так...'
            bot.send_message(message.chat.id, text)
        self.preparation(message)

    def debager(self, message):
        """ Пока использую, как дебагер"""
        self.pl.debug()
        self.out_message(message, self.g.text)

    def preparation(self, message):
        '''Не знаю как правильно завать класс
        вообщем происходят все действия, сохранение и отправка новой сцены
        '''
        self.g.save_game()
        # Обнулим g и pl, во избежании недоразуменй с неочищеной паматью.
        self.g = None
        self.pl = None
        self.create_player(message)
        self.create_game(message)
        self.out_message(message, self.g.text)


BotInstead = Bot()
bot_command = ['start', 'restart']


@bot.message_handler(func=lambda message: True)
def handler(message):
    if message.text.startswith('/'):
        BotInstead.process_command(message)
    else:
        BotInstead.process_message(message)


@bot.callback_query_handler(func=lambda call: True)
def handler_callback(call):
    BotInstead.process_callback(call)


# @bot.message_handler(func=lambda message: True)
# def handler_message(message):
#     BotInstead.process_message(message)


if __name__ == '__main__':
    bot.polling(none_stop=True)
