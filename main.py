import telebot
import config
import pymongo
import re
from telebot import types
from pymongo import MongoClient
from runtime.main import InsteadRuntime

db = MongoClient().instead
bot = telebot.TeleBot(config.token)


class Player():
    '''
    self.callback_value - Значение callback,
    чтобы знать какую клавиатуру показывать.
    self.user_id - id пользователя.
    message - Ответ телеграмма с информацией об отправителе сообщения.
    user_info - Словарь. Содержит информацию о пользователе
        last_name - Фамилия
        first_name - Имя
        username - Логин
    game_info - Словарь. Содержит информацию о игре пользователя.
        auto_save - Автосохранение для Instead.
        callback - Сallback, чтобы знать какую клавиатуру показывать
    '''
    def __init__(self, message):
        '''
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            Создает self.user_id
        '''
        self.user_id = message.from_user.id
        self.inventory = None
        self.callback_value = None

    def search_user(self):
        ''' Осуществляет поиск пользователя в DB
        Returns:
            True : Если пользователь добавлен.
            False : Если пользователь НЕ добавлен.'''
        if db.players.find_one({'_id': self.user_id}):
            print('Player - Пользователь {0} найден.'.format(self.user_id))
            return True
        else:
            print('Player - Пользователь НЕ найден.')
            return False

    def add_user(self):
        ''' Добавляет пользовтеля в DB.
        Returns:
            True : Если пользователь добавлен.
            False : Если пользователь НЕ добавлен.
        '''
        try:
            db.players.insert(self.dict_add)
            print('''Player - Пользователь {0} добавлен
'''.format(self.user_id))
            return True
        except pymongo.errors.DuplicateKeyError:
            print('''Player - Пользователь {0} существует
'''.format(self.user_id))
            return False

    def update_user(self, message):
        ''' Обновляет данные о пользователе (user_info)
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.'''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'user_info.last_name': message.from_user.last_name,
                'user_info.first_name': message.from_user.first_name,
                'user_info.username': message.from_user.username,
            }})
        print('''Player - Личные данные пользователя {0} обновленны
'''.format(self.user_id))

    def reset_game_user(self):
        ''' Обнуляет все данные о игре пользователя (game_info) '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.auto_save': None,
                'game_info.callback': None,
            }})
        print('''Player - Данные о игре пользователя {0} сброшены
'''.format(self.user_id))

    def reset_callback(self):
        ''' Обнуляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.callback': None,
            }})
        self.callback_value = None
        print('''Player - callback обнулен''')

    def update_callback(self, callback_value):
        ''' Обновляет callback '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.callback': callback_value,
            }})
        self.callback_value = callback_value
        print('''Player - callback обновлен''')

    def create_dict_add(self, message):
        ''' Создает словарь для добавления в DB.
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.'''
        user_info = {
            'last_name': message.from_user.last_name,
            'first_name': message.from_user.first_name,
            'username': message.from_user.username,
        }

        game_info = {
            'auto_save': None,
            'callback': None,
        }

        self.dict_add = {
            '_id': self.user_id,
            'user_info': user_info,
            'game_info': game_info,
        }
        print('''Player - Словарь для добавления создан''')

    def save(self, savedata):
        ''' Сохранение игры
        Args:
            savedata : то что возвращает Instead
        '''
        db.players.find_one_and_update(
            {'_id': self.user_id}, {'$set': {
                'game_info.auto_save': savedata,
            }})

    def load(self):
        ''' Загрузка игры из сохранения
        '''
        obj_user = db.players.find_one({'_id': self.user_id})
        savedata = obj_user.get('game_info')
        data = savedata.get('auto_save')
        return data

    def debug(self):
        print('''
Player - ПРОВЕРКА ЗНАЧЕНИЙ
Сallback Value: {0}
Inventory: {1}
'''.format(self.callback_value, self.inventory))


class Room():
    """docstring for Room"""

    action_re = re.compile(r'\{(\w+)\|(\w+)\}')

    def __init__(self, g):
        self.nam = g.instead.title()
        self.no_proccess_dsc = g.instead.full_description()
        self.way = g.instead.ways()

    def debug(self):
        """ Функция для дебага
            Сомтрим интересющие нас состоаяния данного класса."""
        print('''
Room - ПРОВЕРКА ЗНАЧЕНИЙ
Текущая комната: {0}
Название комнаты: {1}
Описание комнаты: {2}
Пути в комнате: {3}
'''.format(self.nam, self.no_proccess_dsc, self.way))

    def process_dsc(self):
        """ Получаем отформатированное описание комнаты """
        print('''Room - Форматируем описание комнаты''')
        self.dsc = self.format_description(self.no_proccess_dsc)
        split_text = self.dsc.split()
        self.dsc = ' '.join(split_text)
        self.dsc = self.dsc.replace("^", "\n")
        self.dsc = self.dsc.replace("--", "—")

    def format_action(self, match):
        ''' Форматирует отдельно взятую команду.
        Args:
            match: совпадение регулярного выражния, объект match
        Returns:
            Строка вида "описание (\name)"
        '''
        return "{0} (/{1})".format(match.group(2), match.group(1))

    def format_description(self, text):
        ''' Форматирует действия в описании сцены и объектов
        Args:
            text (str): Текст, который нужно отформатировать
        Returns:
            Строка, в которой все команды вида "{name|описание}"
             заменены на "описание (\name)"
        '''
        return self.action_re.sub(self.format_action, text)


class Game():
    """docstring for Game"""
    def __init__(self, pl, message):
        self.pl = pl
        self.message = message
        self.instead = InsteadRuntime()
        self.avto_load_game()
        self.inv = self.instead.inv()
        self.rm = Room(self)
        self.pasting_text()

    def pasting_text(self):
        ''' Подготавливает сообщение для отправки в телеграмм
        Returns:
            text (str): Готовое сообщение для отправки
        '''
        print('''Room - Подготавливаем описание комнаты''')
        self.rm.process_dsc()
        self.text = '{0}\n\n{1}'.format(
            self.rm.nam, self.rm.dsc)

    def change_location(self, room_ident):
        ''' Cмена комнаты
        '''
        print(room_ident)
        self.instead.walk(room_ident)

    def avto_load_game(self):
        ''' Загрузка игры из сохранения
        '''
        data = self.pl.load()
        if data:
            self.instead.load(data)
            print('Сохранение загружено')
        else:
            print('Сохранение не найдено')

    def save_game(self):
        ''' Сохранение игры
        '''
        data = self.instead.save()
        self.pl.save(data)
        print('''Game - Игра сохранена''')


class Bot():

    pl = None
    g = None

# start------------------------ Стандартные методы ------------------------

    def create_player(self, message):
        ''' Создает экземпляр пользователя
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            pl(class): Экземпляр пользователя
        '''
        # print(self.pl, self.g)
        if self.pl is None:
            self.pl = Player(message)
            if self.pl.search_user() is True:
                self.pl.update_user(message)
                print('create_player')
            else:
                self.pl.create_dict_add(message)
                self.pl.add_user()

    def create_game(self, message):
        ''' Создает экземпляр игры
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            g(class): Экземпляр игры
        '''
        if self.g is None:
            self.g = Game(self.pl, message)
            print('Bot - создал g')

    def restart_game(self, message):
        ''' Рестарт Игры
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        Returns:
            Обнуляет данные о игре пользователя.
        '''
        self.create_player(message)
        self.pl.reset_game_user()
        self.g = None
        text = 'Для продолжения нажмите /start'
        self.out_message(message, text, message_type='TYPE_NOTIFICATION')

    def create_nam(self, TYPE_NAM):
        """ Cоздает список вида
        {идентификатор: человекочитаемое_название}
            Args: TYPE_NAM - тип создаваемого списка, либо
                пути из комнаты, либо описание предметов.
        """
        if TYPE_NAM == 'WAY':
            self.room_ident = {}
            way = self.g.rm.way
            for item in way:
                room_ident = item[0]
                nam_room = item[1]
                p = {room_ident: nam_room}
                self.room_ident.update(p)
                print(item)
        elif TYPE_NAM == 'INV':
            inv = self.g.inv
            self.inv_ident = {}
            for item in inv:
                inv_ident = item[0]
                nam_inv = item[1]
                p = {inv_ident: nam_inv}
                self.inv_ident.update(p)
                print(item)

    def creating_inline_keyboard(self):
        ''' Создает инлайн клавиатуру
        Returns:
            inkeyboard : Клавиатура для отправки в телеграмм
        '''
        try:
            way = self.g.rm.way
        except AttributeError:
            way = None
        inv = self.g.inv
        print(inv)
        if way and inv:
            inkeyboard = types.InlineKeyboardMarkup()
            locations = types.InlineKeyboardButton(
                text='Смена локации', callback_data="change_location")
            inventory = types.InlineKeyboardButton(
                text='Инвентарь', callback_data="inventory")
            inkeyboard.row(inventory)
            inkeyboard.row(locations)
            print('Bot - инлайн клавиатура создана:', way)
            self.create_nam('WAY')
            self.create_nam('INV')
        elif way:
            inkeyboard = types.InlineKeyboardMarkup()
            locations = types.InlineKeyboardButton(
                text='Смена локации', callback_data="change_location")
            inkeyboard.row(locations)
            print('Bot - инлайн клавиатура создана:', way)
            self.create_nam('WAY')
        else:
            inkeyboard = types.InlineKeyboardMarkup()
            print('Bot - инлайн клавиатура не создана:', way)
        return inkeyboard

    def cleaning_inline_keyboard(self, message):
        '''Убирает инлайн клавиатуру
        '''
        inkeyboard = types.InlineKeyboardMarkup()
        chat_id = message.chat.id
        m_id = self.message_id
        print(m_id)
        try:
            bot.edit_message_reply_markup(
                chat_id=chat_id, reply_markup=inkeyboard, message_id=m_id)
        except Exception:
            pass

    def update_inline_keyboard(self, message):
        '''Обновляет инлайн клавиатуру
        '''
        inkeyboard = self.creating_inline_keyboard()
        chat_id = message.chat.id
        m_id = self.message_id
        try:
            bot.edit_message_reply_markup(
                chat_id=chat_id, reply_markup=inkeyboard, message_id=m_id)
        except Exception:
            pass

    def update_message(self, message):
        '''Обновляет текст сообщения
        '''
        text = self.g.text
        chat_id = message.chat.id
        m_id = self.message_id
        try:
            bot.edit_message_text(
                chat_id=chat_id, text=text, message_id=m_id)
        except Exception:
            pass

    def out_message(self, message, text, message_type='TYPE_ROOM'):
        """ Отправлет сообщения в телеграмм
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
            text(str): Сообщение для отправки в телеграмм
            message_type(str): Тип отправлемого сообщения
        """
        chat = message.chat.id

        if message_type == 'TYPE_ROOM':
            self.message_id = message.message_id + 2
            print(self.message_id, 'ИЗМЕНИТЬ!!!!!!!!!!!!!!!!!!!')
            inkeyboard = self.creating_inline_keyboard()
            bot.send_message(chat, text, reply_markup=inkeyboard)

        elif message_type == 'TYPE_NOTIFICATION':
            keyboard_hider = types.ReplyKeyboardHide()
            bot.send_message(chat, text, reply_markup=keyboard_hider)

# end------------------------ Стандартные методы ------------------------

# start------------------------ Обработка команд ------------------------

    def bot_command(self, message):
        '''Обработка стандартных команд бота

        FIXME: Вынести start в отдельный метод.

        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        '''
        command = message.text[1:]
        if command == 'start':
            message_type = 'TYPE_NOTIFICATION'
            text = 'Игра началась'
            self.out_message(message, text, message_type)
            self.create_player(message)
            self.create_game(message)
            text = self.g.text
            self.out_message(message, text)
        elif command == 'restart':
            self.restart_game(message)

    def game_command(self, message):
        '''Обработка команд Instead

        FIXME: С повторением кода надо что-то делать.
        Так как это вообще не очень то и классно...
        Да и накладно выходит, нехуевая такая нагрузка на БД я думаю.

        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        '''
        command = message.text[1:]
        result = self.g.instead.action(command)
        text = result[0]
        message_type = 'TYPE_NOTIFICATION'
        print(text)

        self.g.save_game()
        self.g = None
        self.pl = None
        self.create_player(message)
        self.create_game(message)

        self.update_message(message)
        self.update_inline_keyboard(message)

        self.g.save_game()
        self.g = None
        self.pl = None
        self.create_player(message)
        self.create_game(message)

        self.out_message(message, text, message_type)

    def process_command(self, message):
        ''' Определяет к какому типу относится команда
        Args:
            message(class): Ответ телеграмма с информацией
            об отправителе сообщения.
        '''
        command = message.text[1:]
        print('command:', command)
        if command in bot_command:
            self.bot_command(message)
        else:
            self.game_command(message)

# end------------------------ Обработка команд ------------------------

# start------------------------ Обработка собщений ------------------------

    def process_callback(self, call):
        """ Обрабатывает полученные сообщения от инлайн клавиатуры """
        self.create_player(call)
        self.create_game(call)
        self.creating_inline_keyboard()
        chat = call.message.chat.id

        if call.data == 'change_location':
            callback_value = 'change_location'
            self.pl.update_callback(callback_value)
            keyboard_edit = types.ReplyKeyboardMarkup(
                one_time_keyboard=True, resize_keyboard=True)

            for item in self.room_ident.values():
                keyboard_edit.add(item)
            text = 'Куда идем?'
            bot.send_message(chat, text, reply_markup=keyboard_edit)
        elif call.data == 'inventory':
            callback_value = 'inventory'
            self.pl.update_callback(callback_value)
            keyboard_edit = types.ReplyKeyboardMarkup(
                one_time_keyboard=True, resize_keyboard=True)
            for item in self.inv_ident.values():
                keyboard_edit.add(item)
            text = 'Что берем?'
            bot.send_message(chat, text, reply_markup=keyboard_edit)

    def process_message(self, message):

        try:
            callback_value = self.pl.callback_value
        except AttributeError:
            self.create_player(message)
            callback_value = self.pl.callback_value

        if callback_value == 'change_location':
            inv_d = {value: key for key, value in self.room_ident.items()}
            print(inv_d)
            if inv_d.get(message.text):
                text = 'Вы перешли в %s' % message.text
                message_type = 'TYPE_NOTIFICATION'
                location = inv_d.get(message.text)
                self.g.change_location(location)
                self.cleaning_inline_keyboard(message)
                self.out_message(message, text, message_type)
            else:
                text = 'Выберете значение с клавиатуры'
                bot.send_message(message.chat.id, text)

            self.g.save_game()
            self.g = None
            self.pl = None
            self.create_player(message)
            self.create_game(message)

            self.out_message(message, self.g.text)

        # elif callback_value == 'inventory':
        #     inv_d = {value: key for key, value in self.inv_ident.items()}
        #     print(inv_d)
        #     if inv_d.get(message.text):
        #         text = '%s' % message.text
        #         message_type = 'TYPE_NOTIFICATION'
        #         location = inv_d.get(message.text)
        #         self.cleaning_inline_keyboard(message)
        #         self.out_message(message, text, message_type)
        #     else:
        #         text = 'Выберете значение с клавиатуры'
        #         bot.send_message(message.chat.id, text)

        #     self.g.save_game()
        #     self.g = None
        #     self.pl = None
        #     self.create_player(message)
        #     self.create_game(message)
        #     self.out_message(message, self.g.text)

        else:
            keyboard_hider = types.ReplyKeyboardHide()
            text = 'Что-то пошло не так...'
            bot.send_message(
                message.chat.id, text, reply_markup=keyboard_hider)

# end------------------------ Обработка собщений ------------------------


BotInstead = Bot()
bot_command = ['start', 'restart']


@bot.message_handler(func=lambda message: True)
def handler(message):
    '''Определяет команда это или сообщение
    '''
    print(message.chat.id, message.message_id)
    if message.text.startswith('/'):
        BotInstead.process_command(message)
    else:
        BotInstead.process_message(message)


@bot.callback_query_handler(func=lambda call: True)
def handler_callback(call):
    '''Отлавливаем callback с инлайн клавиатуры
    '''
    BotInstead.process_callback(call)


if __name__ == '__main__':
    bot.polling(none_stop=True)
