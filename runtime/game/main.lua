-- $Name:Погружение в Instead$
-- $Version:0.3.0$

-- Эта небольшая игра-справочник нацелена помочь познакомиться с основами
-- написания квестов на INSTEAD.
-- Принцип прост: вы изучаете исходный код, который и будете писать само-
-- стоятельно впоследствии. Возможно, сначала он покажется вам несколько
-- сложным, но это первое впечатление быстро проходит :)


-- Перед изучением этого файла рекомендуется ознакомиться с документацией
-- (http://instead.pinebrush.com/wiki/ru/gamedev/documentation). Не лишним
-- будет почитать и pdf-справочник.
-- Полезно держать под рукой справочник по lua (http://www.lua.ru/doc/). А
-- еще полезнее изучить его синтаксис.
-- Впрочем, можете попробовать понять и без этого :) Но рекомендую держать
-- документацию под рукой.


-- Начнем с комментариев.
-- Собственно все, что вы только что прочитали выше, и есть КОММЕНТАРИИ.
-- Комментарии никак не отображаются в игре. Но они полезны для
-- разработчика -- помогают не забыть, за что отвечает тот или иной фрагмент
-- кода.
-- Комментарии начинаются с '--' (два минуса), и действуют до конца текущей
-- строки. Если вы удалите '--' в начале этой строки, и попробуете запустить
-- игру, то INSTEAD выбросит сообщение об ошибке.


-- В самом начале любой игры крайне желательно прописывать ТЕГИ. Теги
-- начинаются с символов комментария. Сейчас их всего три, и они
-- прописаны в заголовке (первые три строчки в этом файле):

-- $Name(ru):Погружение в Instead$  -- Название игры на русском

-- $Version: 0.3.0$                 -- Версия игры. В принципе, этот тег
                                    -- можно не писать.

-- Как вы, возможно, заметили, то что указано в теге '$Name' показывается
-- в диалоге выбора игры.
-- Далее следует указание версии INSTEAD, для которой написана игра:

instead_version "1.6.0"


-- Если вы не хотите, чтобы ваша игра перестала работать на более
-- старых версиях, и ваша игра зависит от instead версии < 1.5.3,
-- то впишите в начало (после instead_version):

if stead.version < "1.5.3" then
    walk = _G["goto"]
    walkin = goin
    walkout = goout
    walkback = goback
end

-- Далее, по традиции, следуют подключения МОДУЛЕЙ.
-- МОДУЛИ добавляют в INSTEAD некую дополнительную функциональность.
-- Вы можете узнать о модулях здесь:
-- http://instead.pinebrush.com/wiki/ru/gamedev/modules
-- Здесь подключим модули xact, hideinv, para, dash и quotes.

require "xact"          -- подключаем модуль xact
require "hideinv"       -- подключаем модуль hideinv
require "para"          -- догадайтесь, что мы подключили в этот раз ;)
require "dash"          -- подключаем dash
require "quotes"        -- подключаем quotes

-- Это не все модули, но нам все и не нужны. Мы взяли простые и часто
-- используемые. :)

-- Модуль xact служит для создания ссылок. С ним разберемся дальше.

-- Модуль hideinv скрывает инвентарь тогда, когда мы захотим.

-- Модули para, dash и quotes служат для красивого вывода текста --
-- para делает красные строки у параграфов, dash заменяет в тексте
-- два минуса на типографское тире (или как там его), а quotes заменяет
-- обычные кавычки на елочки.

game.codepage="UTF-8";

-- В этой строке указываем кодировку текста для нашей игры

game.act = 'Не получается.';
game.inv = 'Гм.. Странная штука..';
game.use = 'Не сработает...';



-- Тремя строчками выше мы определили действия по умолчанию.
-- К примеру, если мы воздействуем лопатой на кирпич, а в игре
-- такого действия не предусмотрено, то выведется строка
-- game.use, то есть 'Не сработает...'. Задавать действия по умолчанию
-- полезно, т.к. игра будет хоть как-то реагировать на неизвестные действия
-- (без действий по умолчанию ничего происходить не будет).
-- Что значат act, inv и use мы разберем далее.

-- А теперь начинаем писать код :)
-- Игра начинается с комнаты main. Ее-то мы и создадим.
-- Если вы чего-то не понимаете, не страшно -- хотя бы "прочувствуйте" язык.
-- Постепенно все будет вставать на свои места, поверьте :)

main = room {
    forcedsc = true,
    nam = "Погружение в INSTEAD",
    dsc = [[Добро пожаловать! В этой небольшой обучающей игре
    вы познакомитесь с основными элементами INSTEAD.^^
    Сейчас вы находитесь в комнате с именем main. Эта комната загружается
    при старте игры.^^
    Комнаты -- похожи. Обычно вверху выводится nam комнаты (Погружение
    в INSTEAD). В середине -- dsc (то, что вы сейчас читаете).
    А затем -- dsc объектов, которые находятся в obj комнаты :)
    Но пока что можете {throw|выбросить} из головы последнее предложение.^^
    Давайте лучше сходим в {next|огород}.]],
    obj = {
        xact('throw', 'Да-да, выбросьте подальше!'),
        xact('next', code[[walk(garden);]]),
    },

    way = {
        'garden',
    },
};

-- Запустите игру, и посмотрите, что получилось.
-- main = room -- этой строчкой мы говорим инстеду, что main будет комнатой.
-- Внутри комнаты (между { } ) прописаны: атрибут forcedsc,
-- название комнаты, ее описание и объекты, находящиеся в комнате.

-- forcedsc = true -- говорит инстеду, что при каком-либо действии
-- скрывать dsc комнаты не нужно. Закомментируйте эту строчку и
-- посмотрите отличия при нажатии на ссылки (по умолчанию forcedsc каждой
-- комнаты установлен в false, то есть описание скрывается).
-- nam -- имя комнаты, выводится в самом верху.
-- dsc -- описание комнаты.
-- obj -- объекты, находящиеся в комнате.

-- Все атрибуты являются сокращенными английскими словами: nam -- name,
-- dsc -- description, obj -- object. Это будет хорошей подсказкой на первых
-- этапах (если вы знаете английский, конечно) -- пробуйте разворачивать
-- атрибуты.

-- В dsc встречаются символы ^. Как вы наверное догадались, это переносы
-- строки. Один ^ эквивалентен одному переносу.

-- Еще одна конструкция, на которой стоит остановиться -- {throw|выбросить}.
-- Это xact-ссылка. throw -- это имя xact, на который мы ссылаемся, а после
-- | идет текст ссылки, который выводится на экран.
-- В описании комнаты есть еще одна ссылка, разберитесь с ней
-- самостоятельно :)

-- Сам xact определяется в obj комнаты, как мы видим это ниже.

-- xact('throw', 'Да-да, выбросьте подальше!'),

-- Слово xact говорит инстеду, что это, в общем-то, xact :)
-- Далее следует имя xact'а (throw, его мы используем в ссылках в описании),
-- а затем текст или команда.
-- В нашем случае это текст:
-- 'Да-да, выбросьте подальше!'
-- При нажатии на ссылку (слово "выбросить" в описании), вы увидите этот
-- текст.

-- В этой комнате есть еще один xact, который немного отличается от
-- предыдущего. Посмотрим на него поближе:

-- xact('next', code[[walk(garden);]])

-- Все как в прошлый раз, кроме code[[walk(garden);]].
-- code говорит инстеду, что нужно выполнить некий код, а не выводить
-- содержимое как текст.

-- walk(garden); -- это переход в указанную комнату. Мы указали garden.
-- В нее и осуществится переход. Поэтому нужно ее создать :)

garden = room {
    nam = "Огород",
    dsc = [[Вы стоите в центре своего великолепного огорода. Солнце приятно
    припекает, бабочки беззаботно кружат в воздухе, а вы с гордостью
    оглядываете результаты вашего многодневного труда -- грядки с огурцами,
    помидорами, луком и укропом. {apples|Яблоня} со спелыми плодами тоже привлекает
    ваш взор.]],
    obj = {
        xact('apples', 'Ей уже целых десять лет!'),
        'woodstick',
    },
    way = {
        'to_appletree',
        'to_bath',
        'to_tomatoes',
    },
};

-- Вот мы и попадаем в огород.
-- Похоже на предыдущую комнату, но добавлено кое-что новое.
-- в obj комнаты есть строчка 'woodstick'. Это значит, что мы поместили в
-- комнату объект woodstick. Сам объект описан чуть ниже.
-- Когда вы просмотрите описание объекта woodstick, фраза в описании
-- main-комнаты должна стать окончательно понятной. :)
-- way содержит в себе список комнат, в которые можно перейти из текущей
-- комнаты. соответственно комнаты: to_appletree, to_bath, to_tomatoes.

woodstick = obj {
    nam = 'Палка',
    dsc = [[Под ногами валяется {палка} метрового размера.]],
    tak = [[Вы подобрали палку.]],
    inv = [[Палка как палка...]],
};

-- Объект woodstick.
-- woodstick = obj -- как и в случае с комнатами. Только теперь вместо
-- room -- obj, ведь мы хотим определить объект :)
-- nam -- имя объекта. Выводится в инвентаре.
-- dsc -- описание объекта. Выводится после dsc комнаты, в которой объект
-- находится :)
-- tak -- хитрый параметр. Если он определен, то объект можно взять,
-- а при его взятии выведется заданная строка. В нашем случае --
-- "Вы подобрали палку".
-- inv -- то, что происходит при использовании объекта в инвентаре самого
-- на себя. В нашем случае выведется строка "Палка как палка...".

-- Пусть нам захочется взять с дерева яблоко :) Для этого мы пойдем к
-- яблоне.

to_appletree = room {
    nam = 'Около яблони',
    dsc = [[Вы подошли к яблоне и скрылись в тени ее раскидистых ветвей.]],
    obj = {
        'apple',
    },
    way = {
        'garden',
    },
};

-- Комната с яблоней. Как можно заметить, она очень похожа на комнату
-- garden, и даже проще.

-- Так как на яблоне висят яблоки ( obj = { 'apple' } ), то определим их.
-- Пусть в игре будет так: когда мы просто нажмем на яблоки, то не сможем
-- их достать, а если попытаемся достать палкой, то яблоко упадет нам по
-- голове, и мы вспомним, что пора бы полить помидоры.

apple = obj {
    var {
        try = false;
    },
    nam = 'Яблоко',
    dsc = [[Над вашей головой качаются десятки спелых {яблок}.]],
    act = [[Вы попытались ухватиться за одно из яблок, но оно слишком
    высоко -- не получается достать. Вы с досадой вздохнули.]],
    used = function (s, w)
        if w == woodstick then
            if s.try == false then
                walk(got_apple);
                s.try = true;
            else
                p [[Вы решили больше не пытаться это сделать. Голову
                жалко.]];
            end;
        end;
    end,
};

-- Здесь появились три новые вещи: var, act и used.

-- var содержит в себе ПЕРЕМЕННЫЕ, относящиеся к объекту.
-- Если слово переменные ни о чем вам не говорит, вспомните математику в
-- школе, а именно всякие иксы и игреки :)
-- Это и есть переменные, ведь мы подставляем в них то, что нам нужно (т.е.
-- они меняются. Само слово -- переменные -- обязывает меняться).

-- В блоке var мы задали переменную try, равную false. Эта переменная будет
-- отображать, пытались ли мы достать палкой яблоко или нет. False означает
-- ложь, то есть с самого начала мы не пытались добыть яблоко :)

-- ВНИМАНИЕ: ставить "=" после слова "var" не нужно! Иначе потом вы будете
-- удивляться странному поведению вашей игры.

-- Немного ниже мы этой переменной воспользуемся.

-- Разберемся с act. Act -- это то, что происходит, когда вы щелкаете на
-- объект в комнате. При этом объект в инвентарь не попадает (в отличие от
-- tak).

-- used -- происходит, когда мы воздействуем чем-то на объект. Если написать
-- здесь просто текст, то он будет выводиться всегда, чем бы мы на яблоки не
-- тыкали. Но нам ведь это не нужно...

-- И тут в игру вступают ФУНКЦИИ. ФУНКЦИИ -- это очень важная часть инстеда,
-- и поняв их, считайте, что писать игры вы научились.

-- Итак...
-- Каждая функция начинается со слова function. Затем, в
-- в круглых скобках, следуют параметры функции. Как правило, первым
-- параметром является сам объект -- по традиции мы условимся называть его
-- s (но можно назвать и brevno, к примеру. Все равно, когда вы в функции
-- будете писать brevno, это значит, что обращаетесь вы к объекту apple).
-- Вторым параметром следует какой-либо посторонний объект. Мы назвали его
-- w. Опять же, традиционно :)

-- Далее мы видим УСЛОВИЕ. Проведем аналогию по-русски:
-- если w == woodstick то
--   ...
-- конец;

-- Более подробно:

-- if               (если)
-- w                (сторонний объект, которым воздействуют в нашем случае)
-- ==               (равен)
-- woodstick        (палке)
-- then             (то)
-- ...              (делаем что-нибудь)
-- end;             (конец условия).

-- Замечание. Если бы мы хотели вместо "равен", записать  "не равен", то мы
-- бы воспользовались конструкцией '~=' вместо '=='. Также, при сравнении
-- числовых переменных, можно пользоваться знаками >, <.

-- В нашем случае, внутри первого условия есть еще одно:

-- if               (если)
-- s.try            (переменная try текущего объекта)
-- ==               (равна)
-- false            (ложь)
-- then             (то)
-- s.try = true;    (устанавливаем переменную try текущего объекта в true)
-- walk(got_apple); (переходим в комнату got_apple)
-- else             (иначе)
-- p [[ ... ]];     (выводим сообщение)
-- end;             (конец условия).

-- Посмотрите, что происходит в игре, и попробуйте разобраться с этим
-- условием сами -- это послужит отличной практикой!

-- После всего этого следует еще один end -- это конец функции.

-- На первый взгляд может показаться запутанным. Но практика все исправит :)

-- Определим комнату got_apple:

got_apple = room {
    nam = 'Около яблони',
    dsc = [[Вы подпрыгнули и попытались сбить яблоко палкой. Но увы,
    промахнулись, а яблоко бесшумно упало, ударило вас по голове и
    укатилось в неизвестном направлении.^^
    Вы сели под деревом, потирая ушибленное место. И тут вас осенило --
    вы забыли полить помидоры!^^
    {polivat|Далее...} ]],
    obj = {
        xact('polivat', code[[set_music('bgm/algar_-_wepp.xm'); walk(to_appletree);]]),
    },
    hideinv = true,
};

-- Похоже на предыдущие комнаты, не так ли?

-- Неизвестная ранее строчка -- set_music('bgm/algar_-_wepp.xm').
-- Этой командой мы проигрываем трек algar_-_wepp.xm,
-- находящийся в папке bgm :)

-- Еще одна незнакомая строчка -- hideinv = true. Этой строчкой мы
-- скрываем инвентарь в комнате. Помните, мы подключили модуль
-- hideinv? Теперь мы им воспользовались.

-- Теперь нам нужно сходить за водой. К старой полусгнившей ванне.
-- Там будет стоять ведро, которое мы должны будем наполнить водой.

to_bath = room {
    nam = 'Около ванны',
    dsc = [[Вы подошли к старой ванне, наполненной водой.
    Вы даже не знаете, кто старше: яблоня или это ржавое корыто...]],
    obj = {
        'water',
        'ground',
        'bucket',
    },
    way = {
        'garden',
    },
};

-- Написанное выше должно быть вам знакомо... :)
-- Определим объекты water и bucket.
-- Water:

water = obj {
    nam = 'Вода',
    dsc = [[В ванне тихо стоит мутная {вода}.]],
    act = [[Мутная вода. Дна ванны не видно.]],
};

-- Ground:

ground = obj {
    nam = 'Земля',
    dsc = function()
        if seen('bucket') then
            p [[На {земле}]];
        else
            p [[На {земле} ничего нет.]];
        end;
    end;
    act = [[Земля, что с нее возьмешь?]],
};

-- dsc земли мы сделали функцией для того, чтобы когда мы подбирали ведро,
-- ее описание изменялось.

-- if                   (если)
-- seen('bucket')       (в комнате есть ведро)
-- then                 (то)
-- p [[ ... ]];         (выводим "На {земле}")
-- else                 (иначе)
-- p [[ ... ]];         (выводим "На {земле} ничего нет.")
-- end;                 (конец)

-- Посмотрите, что происходит в игре, когда мы подбираем ведро с земли :)

-- Bucket:

bucket = obj {
    nam = 'Ведро',
    dsc = [[стоит пустое {ведро}.]],
    tak = [[Вы подняли ведро с земли.]],
    inv = [[Пустое ведро. Для воды.]],
    use = function (s, w)
        if w == water then
            p [[Вы зачерпнули воды из ванны.]];
            put('bucket_full', me());
            remove(s, me());
        else
            p [[Вы подумали и решили, что делать это ведром
            по меньшей мере не солидно.]];
        end;
    end;
};

-- Здесь мы использовали use. Use происходит тогда, когда объект используют
-- на что-то. Use во многом похож на used, который мы использовали выше, в
-- яблоках. Только здесь второй параметр функции -- объект, на который мы
-- воздействуем. А первый, как обычно, сам объект (ведро, в нашем случае).

-- Внутри условия встретились новые команды: put(), remove() и me().
-- Первая добавляет объект куда-то, вторая -- удаляет, а третья возвращает
-- текущего игрока.

-- put(             (добавить)
-- 'bucket_full',   (ведро с водой)
-- me());           (игроку в инвентарь)

-- remove(          (удалить)
-- s,               (сам объект (ведро в нашем случае))
-- me());           (из инвентаря игрока)

-- Определим объект bucket_full:

bucket_full = obj {
    nam = 'Ведро с водой',
    inv = [[Ведро, наполненное водой. Можно идти поливать.]],
    use = function (s, w)
        if w == ground then
            p [[Вы вылили воду на землю. Только зачем...]];
            put('bucket', me());
            remove(s, me());
        elseif w == tomatoes then
            p [[Вы полили помидоры из ведра, и вам показалось, что они
            начали краснеть прямо у вас на глазах.]];
            tomatoes.watered = true;
            replace(s, 'bucket', me());
        else
            p [[Вы не решились этого сделать.]];
        end;
    end,
};

-- Здесь мы снова использовали use, функцию, а также put, remove и me.
-- Но появилось и кое-что новое: elseif и replace.

-- ...
-- elseif               (иначе если)
-- ...

-- То есть, если первое условие не выполняется, иначе проверяется следующее
-- условие.

-- tomatoes.watered = true -- изменяем переменную watered объекта
-- tomatoes на true (watered, как и сам объект tomatoes, мы опишем чуть
-- позже).

-- replace(             (заменить)
-- s,                   (сам объект (ведро с водой, то бишь))
-- 'bucket',            (пустым ведром)
-- me());               (в инвентаре игрока)

-- Проще говоря, replace -- это сокращенная замена того, что мы писали ранее
-- при помощи put и remove :)

-- Заметьте, что я не указывал у этого объекта dsc -- ведь полное ведро
-- никогда не появится в комнатах, следовательно и описание для него не
-- требуется.

-- Так как объекта tomatoes у нас еще нет, создадим теперь его:

tomatoes = obj {
    var {
        watered = true;
    },
    nam = 'Помидоры',
    dsc = function (s)
        if s.watered then
            p [[{Помидоры}, краснея, весело покачиваются из стороны в
            сторону.]];
        else
            p [[{Помидоры} склонились к земле. Кажется, они начинают
            чернеть!]];
        end;
    end;
    act = [[Помидоры... Вы вспоминаете, что обожаете все помидоры.
    Кроме зеленых.]],
};

-- Здесь мы определили переменную watered, которая будет отвечать за
-- полив помидор. По умолчанию она true, то есть помидоры политы.

-- От переменной watered зависит dsc. В условии я использовал сокращенную
-- запись: if s.watered это то же самое, что и if s.watered == true.

-- if                   (если)
-- s.watered            (помидоры политы)
-- then                 (то)
-- p [[ ... ]];         (выводим сообщение 1)
-- else                 (иначе)
-- p [[ ... ]];         (выводим сообщение 2)
-- end;                 (конец)

-- Итак, у помидоров описание меняется в зависимости от того, политы они
-- или нет. А поливаем мы их из ведра с водой -- посмотрите выше.

-- Перейдем к комнате to_tomatoes. Пожалуй, это самая навороченная комната
-- в этой маленькой игре :) Сначала постарайтесь разобраться сами в чем
-- сможете, а потом разберем все по частям. Многие элементы должны быть
-- уже знакомы.

to_tomatoes = room {
    var {
        ent = false;
    },
    entered = function (s)
        if apple.try and not s.ent then
            s.ent = true;
            tomatoes.watered = false;
            p [[Вы подошли к грядке с помидорами. О ужас! Помидоры
            все поникли! Они гибнут, нужно скорее спасти их!]];
        end;
    end,
    nam = 'Около грядки',
    dsc = [[Вы подошли к грядке с помидорами.]],
    obj = {
        'tomatoes',
    };
    way = {
        'garden',
    },
    exit = function (s)
        if s.ent and tomatoes.watered then
            walk(apple_dlg);
        end;
    end,
};

-- Начнем разбираться.
-- В комнате мы задали переменную ent. Она показывает, заходили ли
-- мы в комнату после того, как нам яблоком ударило по голове.
-- По умолчанию -- false.

-- Далее мы видим entered. Entered происходит тогда, когда вы уже вошли в
-- комнату. Здесь мы видим функцию с условием. В условии я использовал
-- сокращения, как и в прошлый раз. Not s.ent это то же самое, что
-- s.ent == false.

-- if                   (если)
-- apple.try            (мы уже попытались сбить яблоко палкой)
-- and                  (и)
-- not s.ent            (ни разу еще после этого в комнату не входили)
-- then                 (то)
-- s.ent = true;        (переменную s.ent устанавливаем в true (то есть мы
--                      один раз уже в комнату вошли))
-- tomatoes.watered     (Переменную watered объекта tomatoes)
-- = false              (устанавливаем в false. Получилось, что они
--                      засохли)
-- p [[ ... ]];         (выводим сообщение)
-- end;                 (конец)

-- nam, dsc, obj, way должны быть вам знакомы по предыдущим комнатам.
-- После них идет exit.
-- Exit происходит тогда, когда вы еще из текущей комнаты не вышли, но
-- намереваетесь уходить в какую-нибудь другую комнату -- нажали
-- на соответствующую ссылку.
-- Здесь мы проверяем, входили ли мы сюда после того, как на голову упало
-- яблоко (s.ent), и политы ли помидоры (tomatoes.watered). Если и то, и
-- другое есть, то переходим в apple_dlg.

-- if                   (если)
-- s.ent                (входили к помидорам, после падения яблока)
-- and                  (и)
-- tomatoes.watered     (полили помидоры)
-- then                 (то)
-- walk(apple_dlg);     (переходим к apple_dlg)
-- end;                 (конец)

-- Опишем apple-dlg.

apple_dlg = dlg {
    entered = [[Уходя с грядки, вы вдруг услышали, как кто-то окликнул
    вас писклявым голосом. Посмотрев под ноги, вы увидели странное яблоко
    с глазами.]],
    nam = 'Разговор с яблоком',
    dsc = [[Яблоко с усмешкой смотрит на вас.]],
    obj = {
        [1] = phr('Эмм, привет!', 'Здравствуй, здравствуй!', [[pon(2,3);]]),
        [2] = _phr('Ты -- говорящее яблоко?', [[А что, ты думал, что
        яблоки разговаривать не умеют?]]),
        [3] = _phr('Эмм... Чего ты хочешь?', [[А ты смышленый
        человек! -- усмехнулось яблоко, -- Я хочу тебя поблагодарить за то,
        что ты сбил меня с этого дерева. Иначе меня б червяки сожрали!]],
        [[pon(4);]]),
        [4] = _phr('Нуу... Пожалуйста.', [[Ага, ну все тогда, я покатился!
        -- сказал Яблоко, и скрылся в неизвестном направлении.]],
        [[pon(5);]]),
        [5] = _phr('...', true, [[walk(the_end);]]),
    };
};

-- apple-dlg это не объект и не комната. Это -- диалог.
-- Любой диалог начинается с dlg.
-- По сути диалог это та же комната, только вместо объектов в ней -- фразы.
-- Впрочем, разберем по порядку.

-- Сначала идет уже знакомый нам entered, выводящий сообщение.
-- Затем nam и dsc диалога. После этого идет obj -- самая важная
-- часть.

-- Как было сказано выше, в obj записаны не объекты, а фразы диалога.
-- Каждая фраза начинается с порядкового номера в квадратных скобках.
-- Например, [1], [2]... Instead выводит фразы в игре по возрастанию.
-- После знака равенства идет phr -- то есть фраза.
-- Рассмотрим ее поближе:

-- phr(                 (фраза)
-- 'Эм, привет!',       (Реплика игрока)
-- 'Здравствуй...',     (Реплика того, с кем игрок говорит)
-- [[pon(2,3)]]),       (Команда -- включить фразы с номерами 2 и 3)

-- Все фразы, кроме первой, записаны чуть по другому: _phr. Это значит,
-- что по умолчанию они СКРЫТЫ, и их нужно показать, выполнив команду
-- pon().

-- Рассмотрим последнюю фразу диалога:

-- _phr(                (фраза)
-- '...',               (реплика игрока)
-- true,                (реплика заменена на true, значит реплики в ответ
--                      игроку нет)
-- [[walk(the_end)]]),  (Команда -- переходим в комнату the_end)

-- Ну и последняя комната, с которой вы легко разберетесь сами ;)

the_end = room {
    nam = 'Конец',
    dsc = [[Вот и конец всей этой коротенькой и бессмысленной истории.
    Но, изучив исходный код этой игры, вы сможете написать свою историю,
    которая будет намного лучше этой... :)^^

    Спасибо!^^

    В игре использована музыка: Algar -- Depression, but why?]],
    hideinv = true,
};

-- Поздравляю! Мы кратко разобрали основы создания квестов на INSTEAD.
-- Если вам помогло данное руководство, то автор безмерно рад.

-- Если же нет, добро пожаловать на форум
-- (http://instead.syscall.ru/forum) или в jabber-конференцию INSTEAD
-- (instead@conference.jabber.ru), где вы легко можете получить
-- ответы на интересующие вас вопросы.

-- Удачи и творческих успехов!
