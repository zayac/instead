#!/usr/bin/env python
import os
import uuid
from operator import is_not
from functools import partial
from lupa import LuaRuntime


class InsteadRuntime:
    game_path = 'game'
    save_path = 'save'
    game_entry_point = 'main'

    def __init__(self):
        self.init_runtime()

    def init_runtime(self):
        '''Создает экземпляр LuaRuntime и подготавливает рантайм игры.
        Данный метод позволяет сбросить текущее состояние при необходимости.
        Следует вызывать, например, чтобы начать игру заново.
        '''
        self.lua = LuaRuntime(unpack_returned_tuples=True)
        self.setup_path()
        self.setup_runtime()
        self.load_player()
        self.load_game()

    def setup_path(self):
        '''Устанавливает в lua пути для поиска импортируемых модулей'''
        self.base_dir = os.path.dirname(os.path.abspath(__file__))

        paths = [
            ';{}'.format(os.path.join(self.base_dir, 'stead', '?.lua')),
            ';{}'.format(os.path.join(self.base_dir, self.game_path, '?.lua')),
        ]

        self.lua.globals().package.path += ''.join(paths)
        self.lua.globals().instead_gamepath = lambda: self.\
            build_fullpath(self.game_path)
        self.lua.globals().instead_savepath = lambda: self.\
            build_fullpath(self.save_path)
        self.lua.globals().instead_realpath = lambda path: self.\
            build_fullpath(path)

    def setup_runtime(self):
        '''Импортирует важные модули Instead и создает функции внутри
        рантайма Lua'''
        self.lua.execute('''
            function table_get_maxn (t)
                local count = 0
                local max = 0
                for k,v in pairs(t) do
                    if k > max then
                        max = k;
                    end
                end
                return max
            end
        ''')
        self.lua.eval("require 'stead'")
        # self.lua.eval("require 'gui'")
        self.lua.execute('game.hinting = true')
        self.lua.execute('pystead = {}')

        # Для нужд автосохранения
        self.lua.execute('original_isEnableSave = isEnableSave;')

    def load_game(self):
        '''Заружает игру в рантайм Lua'''
        self.lua.eval("require '{}'".format(self.game_entry_point))
        self.lua.eval("game.ini(game)")

    def load_player(self):
        '''Создает экземпляр player в рантайме lua'''
        self.lua.execute('''
            pl = player {
                nam = "Incognito";
                where = 'main';
                obj = { };
            };
        ''')

    def where(self):
        '''Текущая комната.

        Returns:
            Строка, идентификатор текущей комнаты
        '''
        return self.lua.eval('stead.me().where')

    def title(self):
        '''Название комнаты'''
        return self.lua.globals().stead.get_title()

    def description(self):
        '''Описание комнаты'''
        return self.lua.eval("stead.call(stead.here(),'dsc')")

    def pic(self):
        '''Картиника сцены.

        TODO: Метод не тестировался. Нужно проверить его работу

        Returns:
            Возвращает строку -- путь к картинке отностильное папки с игрой.
        '''
        return self.lua.eval('stead.get_picture()')

    def full_description(self):
        '''Полное описание комнаты, вместе с объектами.'''
        if not self.lua.globals().pystead.room_full_description:
            self.lua.execute('''
                pystead.object_description = function(self, name)
                    local i, vv, o
                    if isDisabled(self) then
                        return
                    end
                    local v,r = stead.call(self,'dsc');
                    if v then
                        local prep_comand = stead.par(
                            '',
                            stead.par('', '{', stead.deref(self)),
                            '|'
                        );
                        v = stead.string.gsub(v, '{', prep_comand);
                    end
                    for i,o in stead.opairs(self.obj) do
                        o = stead.ref(o);
                        if isObject(o) then
                            vv = stead.obj_look(o);
                            vv = o.dsc;
                            v = stead.par(stead.space_delim, v, vv);
                        end
                    end
                    return v;
                end

                pystead.room_full_description = function(self)
                    local i, vv, o
                    if isDisabled(self) then
                        return
                    end
                    local v = stead.call(self,'dsc');
                    for i,o_name in stead.opairs(self.obj) do
                        o = stead.ref(o_name);
                        if isObject(o) then
                            vv = pystead.object_description(o, o_name);
                            v = stead.par(stead.space_delim, v, vv);
                        end
                    end
                    return v;
                end
            ''')
        return self.lua.eval('pystead.room_full_description(stead.here())')

    def ways(self):
        '''Пути из текущей комнаты

        Returns:
            Список кортежей, состоящих из двух элементов.
            Первый элемент -- идентификатор комнаты, второй -- человекочитаемое
            название.
        '''
        if not self.lua.globals().pystead.room_ways:
            self.lua.execute('''
                pystead.room_ways = function(self)
                    local result = {
                        idents = {},
                        titles = {}
                    };
                    local i = 0;
                    for k,v_name in stead.opairs(self.way) do
                        v = stead.ref(v_name);
                        if isRoom(v) and not isDisabled(v) then
                            result.idents[i] = stead.deref(v)
                            result.titles[i] = stead.call(v, 'nam')
                            i = i + 1;
                        end
                    end
                    return result;
                end
            ''')
        res = self.lua.eval('pystead.room_ways(stead.here())')
        idents = list(res.idents.values())
        titles = list(res.titles.values())
        return list(zip(idents, titles))

    def inv(self):
        '''Инвентарь

        Returns:
            Список кортежей, состоящих из двух элементов.
            Первый элемент -- идентификатор объекта, второй -- человекочитаемое
            название.
        '''
        if not self.lua.globals().pystead.player_inv:
            self.lua.execute('''
                pystead.player_inv = function(inv)
                    local result = {
                        idents = {},
                        titles = {}
                    };
                    local i = 0;
                    for k,v_name in stead.opairs(inv) do
                        v = stead.ref(v_name);
                        if isObject(v) and not isDisabled(v) then
                            result.idents[i] = stead.deref(v)
                            result.titles[i] = stead.call(v, 'nam')
                            i = i + 1;
                        end
                    end
                    return result;
                end
            ''')
        res = self.lua.eval('pystead.player_inv(inv())')
        idents = list(res.idents.values())
        titles = list(res.titles.values())
        return list(zip(idents, titles))

    def use(self, what, onwhat=None, **args):
        '''Позволяет выполнить действие с предметом.с

        Args:
            what (str): Идентификатор предмета, который нужно использовать.
            onwhat (str): К чему применить.
            *args: Назначение этих аргументов науке неизвестно, но в instead
            они есть, пусть и здесь будут.
        Returns:
            Возвращает кортеж из двух элементов. Первый -- строка, если
            действие выполнено или None, если объекты не найдены.
            Второй -- True, если действие выполнено, False, если
            не выполнено и None, если это описание объекта.
        '''
        all_args = filter(partial(is_not, None), [what, onwhat] + list(args))
        method_args = ', '.join(map(lambda i: '"{}"'.format(i), all_args))
        result = self.lua.eval('stead.me():use({})'.format(method_args))
        if type(result) is tuple:
            return result
        else:
            return result, True

    def walk(self, room_ident):
        '''Выполняет переход в другую комнату.

        Args:
            room_ident (str): идентификатор комнаты.
        Returns:
            Результат выполняения команды в Instead.
        '''
        result = self.lua.eval("stead.walk('{}')".format(room_ident))
        self._step()
        return result

    def action(self, action):
        '''Выполняет действие (обычно из числа команд в сцене,
        возможно что-то еще)

        Args:
            action (str): Название действия
        Returns:
            Результат выполнения действия. Обычно кортеж из двух элементов.
        '''
        result = self.lua.eval("pl:action('{}')".format(action))
        self._step()
        return result

    def _step(self):
        self.lua.execute('game:step();')
        self.lua.execute('stead.me():tag()')

    def save(self, force=False):
        '''Сохраняет игру.

        FIXME: Сейчас сохранение записывается в файл со случайным названием,
        Содержимое файла считывается и возвращается, сам файл удаляется.
        Поскольку сохранения задумано хранить в бд, это лишняя нагрузка на
        диск. Как вариант, сделать mock для io.open в lua и все держать в
        памяти.

        Args:
            force (bool): Если True, игнорирует блокировку сохранений.
                Если False, сохранение может быть заблокировано в игровой
                логике.
        Returns:
            Строка. Результат сохранения. По данной строке потом можно
            восстановить состояние при помощи метода load.
        '''
        if force:
            self.lua.globals().isEnableSave = lambda: True
        save_path = os.path.join(self.save_path, str(uuid.uuid4()))
        self.lua.eval('stead.game_save(game, "{}", nil)'.format(
            save_path
        ))
        if force:
            self.lua.execute('isEnableSave = original_isEnableSave')
        savedata = open(save_path, 'r').read()
        os.remove(save_path)
        return savedata

    def load(self, data):
        '''Загружает игру из сохранения.

        FIXME: См. метод save. Работу с файлами нужно заменить на объекты
        в памяти.

        Args:
            data (str): Текст сохранения
        '''
        save_path = os.path.join(self.save_path, str(uuid.uuid4()))
        f = open(save_path, 'w')
        f.write(data)
        f.close()
        self.lua.eval('stead.game_load(game, "{}")'.format(
            save_path
        ))
        os.remove(save_path)

    def build_fullpath(self, path):
        '''Возвращает для заданного относительного пути абсолютный путь с
        учетом местонахождения рантайма.

        Args:
            path (str): Относительный путь.
        Returns:
            Строка, абсолютный путь.
        '''
        return os.path.join(self.base_dir, path)


if __name__ == '__main__':
    instead = InsteadRuntime()

    try:
        with open('save/testing', 'r+') as f:
            instead.load(f.read())
            print('Загрузились из сохранения')
    except OSError:
        pass

    instead.lua.execute('stead.me():tag();')
    while True:
        print(instead.title())
        print(instead.full_description())
        w = instead.ways()
        print('Пути: ', w)
        print('Инвентарь: ', instead.inv())
        action = input('Действие: ')
        if action == 'exit':
            break
        elif action == 'reset':
            instead.init_runtime()
            print('Начнем заново')
        elif action.startswith('go '):
            print(instead.walk(action[3:]))
            continue
        elif action.startswith('use '):
            print(instead.use(*action.split()[1:]))
            continue
        print(instead.action(action))
        input('Жми Enter, чтобы пойти дальше!')
    try:
        with open('save/testing', 'w') as f:
            f.write(instead.save(force=True))
    except OSError:
        print('Не получилось сохранить игру')
